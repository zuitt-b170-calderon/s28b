//Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=> response.json())
.then((json)=> console.log(json));

//Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

fetch("https://jsonplaceholder.typicode.com/todos?/title")
.then((response)=> response.json())
.then((json)=> console.log(json));

//Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=> response.json())
.then((json)=> console.log(json[0]));

 //Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response =>console.log(response.status));

//Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos",{
method:"POST",
headers:{
"Content-Type":"application/json"
},
body:JSON.stringify({
"userId": 11,
"title": "New Title",
"completed": false
})
})
.then((response) => response.json())
.then((json)=> console.log(json));

//Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.


fetch("https://jsonplaceholder.typicode.com/todos/1",{
method:"PUT",
headers:{
"Content-Type":"application/json"
},
body:JSON.stringify({
Title: "Updated Post",
Description: "activity 28",
Status: "incomplete",
Date: "04/21/2022",
userId: 12
})
})
.then((response) => response.json())
.then((json)=> console.log(json));

/*Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
Update a to do list item by changing the status to complete and add a date when the status was changed.*/

fetch("https://jsonplaceholder.typicode.com/todos/1",{
method:"PATCH",
headers:{
"Content-Type":"application/json"
},
body:JSON.stringify({
Status: "complete",
Date: "04/22/2022"
})
})
.then((response) => response.json())
.then((json)=> console.log(json));

//Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1",{
method:"DELETE"
})